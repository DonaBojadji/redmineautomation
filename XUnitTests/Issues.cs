﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Redmine.Net.Api;
using Redmine.Net.Api.Exceptions;
using Redmine.Net.Api.Types;
using Xunit;

namespace XUnitTests
{
    public class Issues
    {
        //set the values 
        const string host = "projects.iborn.net";
        const string apiKey = "f028666b2c29e28bd266ee471a8327ec27189a02";
        const string projectName = "Exchange Rates ";
        [Fact]
        public void ShouldGetIssueById()
        {
            string issueId = "110126";

            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var issue = manager.GetObject<Issue>(issueId, parameters);
            Assert.True(issue.Project.Name.Equals(projectName),
                "Project name is invalid.");
        }

        [Fact]
        public void ShouldGetProjectByName()
        {
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var project = manager.GetObject<Project>("exchange-rates", parameters);
            Assert.True(project.Name.Equals(projectName),
    "Project name is invalid.");

        }

        [Fact]
        public void ShouldGetACustomField()
        {
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            string issueId = "110126";
            var issue = manager.GetObject<Issue>(issueId, parameters);
            var fields = issue.CustomFields;

            var project = manager.GetObject<Project>("exchange-rates", parameters);
            //var custom = manager.GetObjects<CustomField>(parameters);
            var custom = project.CustomFields;
            var kraj = project.CustomFields[1];
            //var fields = manager.GetObjects<CustomField>(parameters);
            var info = kraj.Values;
            Assert.True(project.Name.Equals(projectName),
"Project name is invalid.");

        }


    }
}
