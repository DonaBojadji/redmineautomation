﻿using Redmine.Net.Api;
using Redmine.Net.Api.Exceptions;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Trial.Models
{
    public class RedmineIssue
    {

        NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
        RedmineManager manager = new RedmineManager("testprojects.iborn.net", "9f43ac4ea22514630fad740ae4bc8b34bd022c30");


        public Issue GetIssue(string issueId)
        {
            try
            {
                var issue = manager.GetObject<Issue>(issueId, parameters);
                return issue;
            }
            catch (Exception)
            {
                throw new NotFoundException();
            }
           
        }

        public Issue CreateIssue()
        {

            Issue issue = new Issue
            {
                Project = new IdentifiableName() { Id = 326 },
                Priority = new IdentifiableName() { Id = 6 },
                Subject = "New issue",
                Description = "Some New Description",
                Category = new IdentifiableName() { Id = 1 },
                Status = new IdentifiableName() { Id = 2 },
                AssignedTo = new IdentifiableName() { Id = 246 },
                ParentIssue = null,
                StartDate = System.DateTime.Today,
                DueDate = System.DateTime.Today.AddDays(10)
            };

            var createdIssue = manager.CreateObject<Issue>(issue);
            return createdIssue;
        }


        public Issue DeleteIssue(string issueId)
        {
            manager.DeleteObject<Issue>(issueId);
            return GetIssue(issueId);
        }

        public void UpdateIssue(string issueId)
        {
            var issueToBeUpdated = GetIssue(issueId);
            issueToBeUpdated.Description = "Updated description";
            //anything else you would like to update
            manager.UpdateObject(issueId, issueToBeUpdated);
        }


        public Issue ChangeCustomFieldValue(string issueId)
        {
            var issue = GetIssue(issueId);
            var fields = issue.CustomFields;

            //chane custom field(summary)
            issue.CustomFields[0].Values[0].Info = "This is a short summary that can be changed at any time";

            RedmineProject p = new RedmineProject();
            var possible = p.GetCustomFields();


            IList<CustomFieldValue> addValue = new List<CustomFieldValue>();
            CustomFieldValue value = new CustomFieldValue();
            value.Info = possible[2].PossibleValues[0].Value;
            addValue.Add(value);

            issue.CustomFields[3].Values = addValue;
            issue.Notes = "Just a simple exmple of added notes";
            manager.UpdateObject(issueId, issue);
            var updatedIssue = GetIssue(issueId);

            return updatedIssue;
        }

        //Maybe not possible to add new custom fields
        //Getting an unatorized exception
        public void AddNewIssueCustomField()
        {
            RedmineProject p = new RedmineProject();
            Issue i = new Issue();


        }

        public IList<Issue> GetAllIssuesByAsignee(string asigneeId)
        {
            NameValueCollection n = new NameValueCollection { { RedmineKeys.ASSIGNED_TO_ID, asigneeId } };
            var issues = manager.GetObjects<Issue>(n);
            
            return issues;
        }


        public Issue UploadFileToIssue(string issueId, string documentPath, string fileName)
        {
         
            byte[] documentData = System.IO.File.ReadAllBytes(documentPath);

            Upload attachment = manager.UploadFile(documentData);

            //set attachment properties
            attachment.FileName = fileName;
            attachment.Description = "Picture uploaded using REST API";
      //    attachment.ContentType = "image/png";

            IList<Upload> uploads = new List<Upload>();
            uploads.Add(attachment);

            var wantedIssue = GetIssue(issueId);
            wantedIssue.Uploads = uploads;
            manager.UpdateObject(issueId, wantedIssue);
            var updatedIssue = manager.GetObject<Issue>(issueId, new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.ATTACHMENTS } });


            return updatedIssue;
        }


        public Attachment GetFileById(string fileId)
        {
            var file = manager.GetObject<Attachment>(fileId, null);
            return file;
        }


        public Attachment DeleteFileById(string fileId)
        {
            manager.DeleteObject<Attachment>(fileId);
            return GetFileById(fileId);
        }

        public Issue GetIssueBySubject()
        {

            string subject = "New issue";
            var issues = manager.GetObjects<Issue>(new NameValueCollection { { RedmineKeys.SUBJECT, subject } }).FirstOrDefault();

            return issues;
        }


    }
}