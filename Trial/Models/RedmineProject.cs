﻿using Redmine.Net.Api;
using Redmine.Net.Api.Exceptions;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Trial.Models
{
    public class RedmineProject
    {


        NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
        RedmineManager manager = new RedmineManager("testprojects.iborn.net", "9f43ac4ea22514630fad740ae4bc8b34bd022c30"); 

        public Project CreateProject()
        {
            try
            {
              var project = new Project
                {
                Name = "Project deleted",
                Description = "This is a test project",
                Identifier = "project-deleted",
                HomePage = "https://testprojects.iborn.net/projects",
                IsPublic = true,
                Status = ProjectStatus.Active,
                EnabledModules = new List<ProjectEnabledModule>
                {
                    new ProjectEnabledModule {Name = "issue_tracking"},
                    new ProjectEnabledModule {Name = "time_tracking"}
                }

            };

            var createdProject = manager.CreateObject<Project>(project);
            return createdProject;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        
        }

        public Project GetProject(string projectIdentifier)
        {
            try
            {
              var project = manager.GetObject<Project>(projectIdentifier, parameters);
              return project;
            }
            catch (Exception)
            {
                throw new NotFoundException();
            }
   

        }


        public Project DeleteProject(string projectIdentifier)
        {
                manager.DeleteObject<Project>(projectIdentifier);
                return GetProject(projectIdentifier);
        }

        public IList<CustomField> GetCustomFields()
        {
            var customFields = manager.GetObjects<CustomField>();
            return customFields;
        }

        //successfully updates the custom field
        //it's possible to add new values but only if they are firstly added in possible values
        public Project ChangeCustomFieldValue(string projectIdentifier)
        {

            var project  = GetProject(projectIdentifier);
            var fields = project.CustomFields;
            var possibleFields = GetCustomFields();
       
            //Client
            IList<CustomFieldValue> list = new List<CustomFieldValue>();
            CustomFieldValue client = new CustomFieldValue();
            client.Info = possibleFields[11].PossibleValues[2].Value;
            list.Add(client);
            project.CustomFields[0].Values = list;


            //Risk Category
            IList<CustomFieldValue> list12 = new List<CustomFieldValue>();
            CustomFieldValue riskCategory = new CustomFieldValue();
            riskCategory.Info = possibleFields[14].PossibleValues[1].Value;
            list12.Add(riskCategory);
            project.CustomFields[1].Values = list12;
            manager.UpdateObject(projectIdentifier, project);

            //Budget
            IList<CustomFieldValue> list1 = new List<CustomFieldValue>();
            CustomFieldValue budget = new CustomFieldValue();
            budget.Info = "100";
            list1.Add(budget);
            project.CustomFields[2].Values = list1;
            manager.UpdateObject<Project>(projectIdentifier, project);

            //Recuring Budget
            IList<CustomFieldValue> list2 = new List<CustomFieldValue>();
            CustomFieldValue recuringBudget = new CustomFieldValue();
            recuringBudget.Info = possibleFields[19].PossibleValues[0].Value;
            list2.Add(recuringBudget);
            project.CustomFields[3].Values = list2;
            manager.UpdateObject(projectIdentifier, project);

            //Start date
            IList<CustomFieldValue> list3 = new List<CustomFieldValue>();
            CustomFieldValue startDate = new CustomFieldValue();
            string date = DateTime.Today.ToString("yyyy-MM-dd");

            startDate.Info = date;
            list3.Add(startDate);
            project.CustomFields[4].Values = list3;


            //End date
            IList<CustomFieldValue> list4 = new List<CustomFieldValue>();
            CustomFieldValue endDate = new CustomFieldValue();
            string enddate = DateTime.Today.AddDays(10).ToString("yyyy-MM-dd");
            endDate.Info = enddate;
            list4.Add(endDate);
            project.CustomFields[5].Values = list4;


            //Status
            IList<CustomFieldValue> list5 = new List<CustomFieldValue>();
            CustomFieldValue status = new CustomFieldValue();
            status.Info = possibleFields[18].PossibleValues[2].Value;
            list5.Add(status);
            project.CustomFields[6].Values = list5;

            //Responsible
            IList<CustomFieldValue> list6 = new List<CustomFieldValue>();
            CustomFieldValue responsible = new CustomFieldValue();
            responsible.Info = "243";
            list6.Add(responsible);
            project.CustomFields[7].Values = list6;


            manager.UpdateObject(projectIdentifier, project);
            var updatedProject = GetProject(projectIdentifier);
            return updatedProject;
        }


        //not working function
        //getting unautorized error
        public void AddPossibleValuesInCustomFields()
        {
            var fields = GetCustomFields();
            IList<CustomFieldPossibleValue> customFieldPossible = new List<CustomFieldPossibleValue>();
            CustomFieldPossibleValue val = new CustomFieldPossibleValue();
            val.Value = "D";
            customFieldPossible.Add(val);
            fields[14].PossibleValues.Add(val);
            manager.UpdateObject("17", fields[14]);

        }


    }

}