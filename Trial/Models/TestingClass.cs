﻿using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Trial.Models
{
    public class TestingClass
    {

        public Issue GetIssue(string issueId)
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var issue = manager.GetObject<Issue>(issueId, parameters);

            return issue;
        }

        public void CreateIssue()
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);

            Issue issue = new Issue
            {
                Project = new IdentifiableName() { Id = 334 },
                Priority = new IdentifiableName() { Id = 6 },
                Subject = "Newly created issue",
                Description = "Some Description",
                Category = new IdentifiableName() { Id = 1 },
                Status = new IdentifiableName() { Id = 2 },
                AssignedTo = new IdentifiableName() { Id = 243 },
                ParentIssue = null,
                CustomFields = new List<IssueCustomField>
                {
                    new IssueCustomField
                    {
                        //Id we put in for the custom field
                        Id = 13,
                        Values = new List<CustomFieldValue> {new CustomFieldValue {Info = "This should be a custom field"}}
                    }
                },
                StartDate = System.DateTime.Today,
                DueDate = System.DateTime.Today.AddDays(10)
                
                
            };
      

            Issue savedIssue = manager.CreateObject(issue);
        }


        public void UpdateIssueCustomField(string issueId)
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.NEW_VALUE } };
            RedmineManager manager = new RedmineManager(host, apiKey);

            var issueUpdate = manager.GetObject<Issue>(issueId, null);

            CustomFieldValue v = new CustomFieldValue();
            v.Info = "PleaseChange";
            IList<CustomFieldValue> li = new List<CustomFieldValue>();
            li.Add(v);
            issueUpdate.CustomFields[2].Values[0] = v;
            var field = issueUpdate.CustomFields;
            

            manager.UpdateObject(issueId, issueUpdate);
        }

        public Project GetProject(string projectName)
        {
            string host = "testprojects.iborn.net";
            string apiKey = "9f43ac4ea22514630fad740ae4bc8b34bd022c30";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var project = manager.GetObject<Project>(projectName, parameters);

            var customFields = manager.GetObjects<CustomField>();
                return project;
        }

        public void CreateProject()
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);

            var project = new Project
            {
                Name = "New Test Project",
                Description = "This project is made using the Redmine API",
                Identifier = "new-test-project",
                HomePage = "testprojects.iborn.net",
                IsPublic = true,
                Status = ProjectStatus.Active,
                Trackers = new List<ProjectTracker>
                {
                    new ProjectTracker {Id = 1},//some id of a member
                    new ProjectTracker {Id = 2}//another id of a memeber
                }
            };

            var savedProject = manager.CreateObject(project);

        }

        public IList<IssueCustomField> GetCustomFieldsOnProject(string projectName)
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var project = manager.GetObject<Project>(projectName, parameters);

            return project.CustomFields;
        }

        public void CreateCustomFieldISBN()
        {
            string host = "testprojects.iborn.net";
            string apiKey = "3be9d6628caf746c88220ba3b1533285669dcfa6";
            string issueId = "91357";
            NameValueCollection parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.RELATIONS } };
            RedmineManager manager = new RedmineManager(host, apiKey);
            var issue = manager.GetObject<Issue>(issueId, parameters);
            CustomFieldValue v = new CustomFieldValue();
            v.Info = "";
            IssueCustomField issueCustom = new IssueCustomField();

            IList<CustomFieldValue> lista = new List<CustomFieldValue>();
            lista.Add(v);


            issueCustom.Id = 10;
            issueCustom.Name = "ISBN";
            issueCustom.Multiple = false;
            issueCustom.Values = lista;
            issue.CustomFields.Add(issueCustom);
            //Don't forget to update!
            //It should work

            manager.UpdateObject(issueId, issue);

        }




        public void UploadAttachment()
        {
            RedmineManager redmineManager = new RedmineManager("testprojects.testprojects.iborn.net", "3be9d6628caf746c88220ba3b1533285669dcfa6");
            //read document from a specified path
            string documentPath = "C:\\Users\\donab\\Desktop\\tryone.doc";
            byte[] documentData = System.IO.File.ReadAllBytes(documentPath);

            //upload attachment to redmine
            Upload attachment = redmineManager.UploadFile(documentData);

            //set attachment properties
            attachment.FileName = "SomeFile.txt";
            attachment.Description = "File uploaded using REST API";
            attachment.ContentType = "text/plain";

            //create a list of attachments to be added to issue
            IList<Upload> attachments = new List<Upload>();
            attachments.Add(attachment);

            //get project
            var projects = redmineManager.GetObjects<Project>(new NameValueCollection());
            var project = projects.OfType<IdentifiableName>()
                                   .Where(p => p.Name.Equals("Exchange Rates "))
                                   .FirstOrDefault();

            //create issue and attach the document
            var issusID = "110150";
            var issueUpdate = redmineManager.GetObject<Issue>(issusID, null);
            issueUpdate.Uploads = attachments;
            issueUpdate.DueDate = new DateTime(2018, 2, 18);
            redmineManager.UpdateObject(issusID, issueUpdate);
            //Issue issueWithAttachment = redmineManager.CreateObject<Issue>(
            //                                new Issue
            //                                {
            //                                    Project = project,
            //                                    Subject = "Issue with attachment",
            //                                    Uploads = attachments
            //                                });
        }
    }
}