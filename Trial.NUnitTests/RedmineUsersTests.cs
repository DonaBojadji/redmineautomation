﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Trial.Models;

namespace Trial.NUnitTests
{
    [TestFixture]
    public class RedmineUsersTests
    {

        [Test]
        [Ignore("Don't create any users for now")]
        public void ShouldCreateUser()
        {
            RedmineUsers redmineUsers = new RedmineUsers();
            var user = redmineUsers.CreateUser();
            Assert.NotNull(user);

        }
    }
}
