﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Redmine.Net.Api.Types;
using Trial.Models;
using Redmine.Net.Api.Exceptions;

namespace Trial.NUnitTests
{

    [TestFixture]
    public class RedmineProjectTests
    {
        [Test]
        public void ShouldCreateProject()
        {
            RedmineProject c = new RedmineProject();
            Project p = c.CreateProject();
            Assert.NotNull(p, "Project was created successfully");
            Assert.True(p.Identifier.Equals("project-deleted"), "Identifier is invalid");
            Assert.True(p.Name.Equals("Project deleted"), "Name is invalid");
        }

        [Test]
        public void ShouldGetProject()
        {
            RedmineProject redmine = new RedmineProject();
            Project p = redmine.GetProject("project-created-using-redmine-api");
            Assert.NotNull(p);
            Assert.AreEqual(p.Name, "Project created using Redmine API");
            Assert.True(p.Identifier.Equals("project-created-using-redmine-api"), "Project name is invalid");
            Assert.True(p.IsPublic.Equals(true), "IsPublic field is invalid");
            Assert.True(p.Status.Equals(ProjectStatus.Active), "Project status is invalid");
            Assert.True(p.Description.Equals("This is a test project"), "Project description is invalid");
        }

        [Test]
        public void ShouldDeleteProject()
        {
            RedmineProject redmine = new RedmineProject();
            string projectIdentifier = "project-deleted";
            Assert.That(() => redmine.DeleteProject(projectIdentifier), Throws.TypeOf<NotFoundException>());
        }

        [Test]
        public void ShouldGetCustomFields()
        {
            RedmineProject redmine = new RedmineProject();
            var fields = redmine.GetCustomFields();
            Assert.NotNull(fields);
            Assert.AreEqual(fields.Count(), 23);
        }


        [Test]
        public void ShouldChangeProjectCustomFieldValue()
        {
            string projectIdentifier = "project-created-using-redmine-api";
            RedmineProject redmine = new RedmineProject();
            var project =  redmine.ChangeCustomFieldValue(projectIdentifier);
            Assert.NotNull(project);
          //  Assert.AreEqual(project.CustomFields[1].Values[0].Info, "B");
        }


        [Test]
        [Ignore("Not working function")]
        public void ShouldAddPossibleValuesInCustomFields()
        {
            RedmineProject redmine = new RedmineProject();
            Assert.Throws<NotImplementedException>(() => redmine.AddPossibleValuesInCustomFields());
        }

    }
}
