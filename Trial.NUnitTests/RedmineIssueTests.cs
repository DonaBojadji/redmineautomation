﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Trial.Models;
using Redmine.Net.Api.Types;
using Redmine.Net.Api.Exceptions;

namespace Trial.NUnitTests
{

    [TestFixture]
    public class RedmineIssueTests
    {
        [Test]
        public void ShouldGetIssue()
        {
            string issueId = "91660";
            int b = Convert.ToInt32(issueId);

            RedmineIssue redmineIssue = new RedmineIssue();
            Issue issue = redmineIssue.GetIssue(issueId);

            Assert.NotNull(issue, "Successully got the issue");
            Assert.True(issue.Id.Equals(b), "Issue Id is invalid");
            Assert.True(issue.Description.Equals("Updated description"), "Description is invalid");

        }

        //not necessary needed
        //[Test]
        //[TestCase("91660123")]
        //public void ShouldGetIssueException(string issueId)
        //{
        //    RedmineIssue issue = new RedmineIssue();
        //    Assert.That(() => issue.GetIssue(issueId), Throws.TypeOf<NotFoundException>());
        //}


        [Test]
        public void ShouldCreateIssue()
        {
            RedmineIssue issue = new RedmineIssue();
            var createdIssue = issue.CreateIssue();
            Assert.NotNull(createdIssue, "Successully created issue");
            Assert.True(createdIssue.Description.Equals("Some New Description"), "Invalid issue description");
            Assert.True(createdIssue.Subject.Equals("New issue"), "Issue subject is invalid");
        }

        [Test]
        public void ShouldDeleteIssue()
        { 
            RedmineIssue IssueMethod = new RedmineIssue();
            var isssue = IssueMethod.GetIssueBySubject();
            var issueId = Convert.ToString(isssue.Id);
            Assert.That(() => IssueMethod.DeleteIssue(issueId),Throws.TypeOf<NotFoundException>());
        }

        [Test]
        public void ShouldUpdateIssue()
        {
            string issueId = "91660";
            RedmineIssue IssueMethod = new RedmineIssue();
            IssueMethod.UpdateIssue(issueId);
            Assert.AreEqual("Updated description", IssueMethod.GetIssue(issueId).Description);
        }

        [Test]
        public void ShouldChangeIssueCustomField()
        {
            var issueId = "91660";
            int id = Convert.ToInt32(issueId);
            RedmineIssue issueMethod = new RedmineIssue();
            var updatedIssue = issueMethod.ChangeCustomFieldValue(issueId);
            Assert.NotNull(updatedIssue);
            Assert.AreEqual(updatedIssue.Id, id);
            Assert.IsNotEmpty(updatedIssue.CustomFields[0].Values[0].Info);
        }

        //failing test 
        [Test]
        [Ignore("Not working function")]
        public void ShouldAddNewIssueCustomField()
        {
            RedmineIssue issueMethod = new RedmineIssue();
            Assert.Throws<NotImplementedException>(() => issueMethod.AddNewIssueCustomField());
        }


        [Test]
        public void ShouldGetAllIssuesByAsignee()
        {
            string asigneeId = "246";
            int asignedTo = Convert.ToInt32(asigneeId);

            RedmineIssue redmineIssue = new RedmineIssue();
            var issues = redmineIssue.GetAllIssuesByAsignee(asigneeId);

            Assert.NotNull(issues);
            Assert.NotZero(issues.Count());
            Assert.AreEqual(asignedTo, issues[0].AssignedTo.Id);
            
        }

        
        [Test]
        [Ignore("Just for now stop creating more files")]
        public void ShouldUploadFileToIssue()
        {
            string issueId = "91660";
            string documentPath = "C:\\Users\\donab\\Desktop\\example.png";
            string fileName = "example.png";

            RedmineIssue redmineIssue = new RedmineIssue();
            var issue = redmineIssue.UploadFileToIssue(issueId, documentPath, fileName);

            Assert.NotNull(issue);
            Assert.NotZero(issue.Attachments.Count());
            Assert.AreEqual("example.png", issue.Attachments[issue.Attachments.Count()-1].FileName);
        }

        [Test]
        public void ShouldGetFileById()
        {
            string fileId = "10284";
            RedmineIssue redmineIssue = new RedmineIssue();
            var file = redmineIssue.GetFileById(fileId);

            Assert.NotNull(file);
            Assert.AreEqual(file.FileName, "exmple.png");
        }

        [Test]
        public void ShouldDeleteFileById()
        {
            string fileId = "10304";
            RedmineIssue redmineIssue = new RedmineIssue();
            Assert.That(() => redmineIssue.DeleteFileById(fileId), Throws.TypeOf<NotFoundException>());
        }
       
        [Test]
        public void ShoudGetIssueBySubject()
        {
            RedmineIssue redmineIssue = new RedmineIssue();
            var issue = redmineIssue.GetIssueBySubject();

            Assert.NotNull(issue);
            Assert.That(issue.Subject.Equals("New issue"));
        }
    }
    
}
